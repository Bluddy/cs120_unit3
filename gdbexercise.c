/* This code contains many little examples of data types, memory
   usage, and function calling, with some intentional errors.  We
   recommend running through gdb as an exercise in debugging and
   understanding what's going on with various data types and storage
   classes.
*/

#include <stdio.h>

int sum(char array[], int size)
{
    int result = 0;
    for (int i=0; ; i++)
        result += array[i];
    return result;
}

int factorial(int n)
{
    int result;
    result = n * factorial(n-1);
    return result;
}

int main()
{
    unsigned char array[] = { 70, 4, 200, 16, 99 };
    
    // Let's print the number 7
    char num1 = 2;
    char num2 = 5;
    char number = num1 + num2;
    printf("number = %c\n", number); // what's wrong with this?

    //sizeof(array) gives you the number of bytes allocated for array
    //ONLY IF array is declared with a fixed size (not a pointer, no malloc)
    int result = sum(array, sizeof(array) / sizeof(array[0]));

    // trace this call
    printf("4! = %d\n", factorial(4));

    int data[10];
    // how can we make this loop do less work?
    int i;
    for (i=0; i < sizeof(data) / sizeof(data[0]); i++)
        data[i] = i;

    // compare: does this version actually compile to fewer instructions?
    int len = sizeof(data) / sizeof(data[0]);
    for (i=0; i < len; data[i] = i, i++);

    printf("data initialized\n");

    len = 1000; 
    for (int i=0; i < len; i++) {
      // results not deterministic, 
      // sometimes but not always crashes when run from the command line
      result = data[i];
    }

    int t = 7;
    unsigned int st = -2; //Nope
    unsigned thisisanint = 2000; //Yes!
    //char is between -128 and 127
    char c = 200; //No!
    unsigned char uc = -1; //Nope

    char binary = 0b10000000;

    printf("\n%d, %u, %u, %c, %c, %c\n", t, st, thisisanint, c, uc, binary);

    return 0;
}
