#include <stdio.h>
#include <math.h>
#include <stdbool.h>

#define DIMX 100
#define DIMY 20
#define YOFFSET (DIMY / 2)
#define PI 3.14159265359
#define FUN(x) sin(x)

int main() {
  bool result[DIMY][DIMX] = {0};
  double xdelta = 2 * PI / (1 * DIMX);
  double ydelta = 2.0 / DIMY;
  for (int x=0; x < DIMX; x++) {
    double v = FUN(xdelta * x);
    for (int y=0; y < DIMY; y++) {
      if (v >= (y - YOFFSET) * ydelta && v < (y - YOFFSET + 1) * ydelta) {
        result[y][x] = true;
      }
    }
  }
  // print out the graph
  return 0;
}
